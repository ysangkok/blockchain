#ifndef DEBUG_VISUALIZE_H

#define DEBUG_VISUALIZE_H

#include <stdint.h>
#include "BlockChain.h"
// This is a windows only feature that allows for full debug visualization output of some of the blockchain data.

class UsageStat
{
public:
	UsageStat(void)
	{
		mDescription[0] = 0;
		mValue = 0;
		mCount = 0;
	}
	char		mDescription[512];
	uint32_t	mValue;
	uint32_t	mCount;
};

class DebugVisualize
{
public:

	virtual void usageChart(const char *title1,
		const char *title2,
		uint32_t count,
		const UsageStat *stats1,
		const UsageStat *stats2) = 0;


	virtual void usageChart(const char *title1,
							const char *title2,
							const char *title3,
							uint32_t count,
							const UsageStat *stats) = 0;

	virtual void processZombieReport(const char *dateString,const BlockChain::ZombieReport &report) = 0;


	virtual void release(void) = 0;

protected:
	virtual ~DebugVisualize(void)
	{

	}
};

DebugVisualize *createDebugVisualize(void);

#endif

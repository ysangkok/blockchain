#include "DebugVisualize.h"
#include "RenderDebugBinding.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifdef _MSC_VER
#pragma warning(disable:4996 4100)
#endif

void getRenderDebugDLL(void)
{
	printf("Loading RenderDebug DLL\r\n");
	RENDER_DEBUG::RenderDebug::Desc desc;
	desc.dllName = "RenderDebug_x64.dll";
	desc.recordFileName = "BlockChain.rec";
	desc.runMode = RENDER_DEBUG::RenderDebug::RM_FILE;
	gRenderDebug = RENDER_DEBUG::createRenderDebug(desc);
}

#define MAXNUMERIC 32  // JWR  support up to 16 32 character long numeric formated strings
#define MAXFNUM    16

static	char  gFormat[MAXNUMERIC*MAXFNUM];
static int32_t    gIndex=0;

static const char * formatNumber(int32_t number) // JWR  format this integer into a fancy comma delimited string
{
	char * dest = &gFormat[gIndex*MAXNUMERIC];
	gIndex++;
	if ( gIndex == MAXFNUM ) gIndex = 0;

	char scratch[512];

#ifdef _MSC_VER
	itoa(number,scratch,10);
#else
	snprintf(scratch, 10, "%d", number);
#endif

	char *source = scratch;
	char *str = dest;
	uint32_t len = (uint32_t)strlen(scratch);
	if ( scratch[0] == '-' )
	{
		*str++ = '-';
		source++;
		len--;
	}
	for (uint32_t i=0; i<len; i++)
	{
		int32_t place = (len-1)-i;
		*str++ = source[i];
		if ( place && (place%3) == 0 ) *str++ = ',';
	}
	*str = 0;

	return dest;
}


class DebugVisualizeImpl : public DebugVisualize
{
public:
	DebugVisualizeImpl(void)
	{
		mFph = fopen("Usage.csv","wb");
		mZombieReport = NULL;
		mFirst = true;
	}

	virtual ~DebugVisualizeImpl(void)
	{
		if ( mFph )
		{
			fclose(mFph);
		}
		if ( mZombieReport )
		{
			fclose(mZombieReport);
		}
	}

	virtual void usageChart(const char *title1,
							const char *title2,
							const char *title3,
						    uint32_t count,
							const UsageStat *stats)
	{

		if ( mFph )
		{
			if ( mFirst )
			{
				mFirst = false;
				fprintf(mFph,"Date,");
				for (uint32_t i=0; i<count; i++)
				{
					fprintf(mFph,"\"%s\",", stats[i].mDescription );
				}
				fprintf(mFph,"\r\n");
			}
			fprintf(mFph,"\"%s\",",title1);
			for (uint32_t i=0; i<count; i++)
			{
				fprintf(mFph,"\"%d\",", stats[i].mValue);
			}
			fprintf(mFph,"\r\n");
			fflush(mFph);
		}

		for (uint32_t j=0; j<3; j++)
		{

			const float BAR_SIZE = 1.5;

			uint32_t c = RENDER_DEBUG::DebugColors::Red;

			gRenderDebug->addToCurrentState(RENDER_DEBUG::DebugRenderState::SolidShaded);
			gRenderDebug->addToCurrentState(RENDER_DEBUG::DebugRenderState::CenterText);
			gRenderDebug->setCurrentTextScale(0.5f);

			uint32_t totalCoins = 0;
			uint32_t totalCount = 0;

			for (uint32_t i=0; i<count; i++)
			{
				const UsageStat &u = stats[i];
				totalCoins+=u.mValue;
				totalCount+=u.mCount;
			}

			float graphWidth = (count+2)*BAR_SIZE+BAR_SIZE;

			float startX=0;

			switch ( j )
			{
				case 0:
					startX = -graphWidth;
					break;
				case 1:
					startX = 0;
					break;
				case 2:
					startX = graphWidth;
					break;
			}

			float center = startX + graphWidth*0.5f;

			{
				float bmin[3];
				float bmax[3];

				bmin[0] = startX+BAR_SIZE;
				bmax[0] = startX+graphWidth-BAR_SIZE;

				bmin[1] = 10.0f;
				bmax[1] = 11.0f;

				bmin[2] = -BAR_SIZE;
				bmax[2] = -0.01f;

				gRenderDebug->setCurrentColor(0x505050,0xFFFFFF);
				gRenderDebug->debugBound(bmin,bmax);

				bmin[1] = -0.3f;
				bmax[1] = -0.01f;
				gRenderDebug->debugBound(bmin,bmax);


			}

			{
				float pos[3];
				pos[0] = center;
				pos[1] = 10.5;
				pos[2] = 0.1f;
				gRenderDebug->setCurrentColor(0xFFFFFF,0xFFFFFF);
				gRenderDebug->setCurrentTextScale(2.0f);
				switch ( j )
				{
				case 0:
					gRenderDebug->debugText(pos,title1);
					break;
				case 1:
					gRenderDebug->debugText(pos,title2);
					break;
				case 2:
					gRenderDebug->debugText(pos,title3);
					break;
				}
				char scratch[512];
				sprintf(scratch,"Total Bitcoins: %s Total Non-Zero Addresses: %s", formatNumber(totalCoins), formatNumber(totalCount));
				pos[0] = center;
				pos[1] = 10.1;
				pos[2] = 0.1f;
				gRenderDebug->setCurrentColor(0xFFFF00,0xFFFFFF);
				gRenderDebug->setCurrentTextScale(1.5f);
				gRenderDebug->debugText(pos,scratch);
			}

			gRenderDebug->setCurrentTextScale(0.5f);

			for (uint32_t i=0; i<count; i++)
			{
				const UsageStat &u = stats[i];

				float percent=0;
				float total=0;

				switch ( j )
				{
					case 0:
						percent = (float)u.mValue / (float) totalCoins;
						total = (float)u.mValue / 21000000.0f;
						break;
					case 1:
						percent = (float)u.mCount / (float) totalCount;
						total = (float)u.mValue / 21000000.0f;
						break;
					case 2:
						percent = (float)u.mCount / (float) totalCount;
						total = percent;
						break;
				}

				float x = i*BAR_SIZE+startX+BAR_SIZE;
				float wid = BAR_SIZE-(BAR_SIZE*0.1f);

				float bmin[3];
				float bmax[3];

				bmin[0] = x;
				bmax[0] = x+wid;

				bmin[1] = 0;
				bmax[1] = total*20.0f;

				bmin[2] = -wid;
				bmax[2] = 0;

				uint32_t color = gRenderDebug->getDebugColor((RENDER_DEBUG::DebugColors::Enum)c);
				gRenderDebug->setCurrentColor(color,0xFFFFFF);
				gRenderDebug->debugBound(bmin,bmax);

			
				gRenderDebug->setCurrentColor(0xFFFFFF,0xFFFFFF);
				float pos[3];
				pos[0] = bmin[0]+BAR_SIZE*0.5f;
				pos[1] = -0.2f;
				pos[2] = 0.1f;
				gRenderDebug->debugText(pos,u.mDescription);

				pos[1] = 0.1f;
				char scratch[512];
				sprintf(scratch,"%0.1f percent", percent*100 );
				gRenderDebug->debugText(pos,scratch);

				pos[1] = 0.3f;
				sprintf(scratch,"%s bitcoins", formatNumber(u.mValue) );
				gRenderDebug->debugText(pos,scratch);

				pos[1] = 0.5f;
				sprintf(scratch,"%s addresses", formatNumber(u.mCount) );
				gRenderDebug->debugText(pos,scratch);


				c++;
				if ( c == RENDER_DEBUG::DebugColors::NUM_COLORS )
				{
					c = RENDER_DEBUG::DebugColors::Red;
				}
			}
		}
		gRenderDebug->render(1.0f/60.0f,NULL);
	}

	virtual void usageChart(const char *title1,
		const char *title2,
		uint32_t count,
		const UsageStat *stats1,
		const UsageStat *stats2) 
	{
		for (uint32_t j=0; j<1; j++)
		{

			const float BAR_SIZE = 0.3f;

			uint32_t c = RENDER_DEBUG::DebugColors::Red;

			gRenderDebug->addToCurrentState(RENDER_DEBUG::DebugRenderState::SolidShaded);
			gRenderDebug->addToCurrentState(RENDER_DEBUG::DebugRenderState::CenterText);
			gRenderDebug->setCurrentTextScale(0.5f);

			uint32_t totalCoins = 0;
			uint32_t totalCount = 0;

			for (uint32_t i=0; i<count; i++)
			{
				const UsageStat &u = stats1[i];
				totalCoins+=u.mValue;
				totalCount+=u.mCount;
			}

			float graphWidth = (count+2)*BAR_SIZE+BAR_SIZE;

			float startX=0;

			switch ( j )
			{
			case 0:
				startX = -graphWidth;
				break;
			case 1:
				startX = 0;
				break;
			}

			float center = startX + graphWidth*0.5f;

			{
				float bmin[3];
				float bmax[3];

				bmin[0] = startX+BAR_SIZE;
				bmax[0] = startX+graphWidth-BAR_SIZE;

				bmin[1] = 10.0f;
				bmax[1] = 11.0f;

				bmin[2] = -BAR_SIZE;
				bmax[2] = -0.01f;

				gRenderDebug->setCurrentColor(0x505050,0xFFFFFF);
				gRenderDebug->debugBound(bmin,bmax);

				bmin[1] = -0.3f;
				bmax[1] = -0.01f;
				gRenderDebug->debugBound(bmin,bmax);


			}

			{
				float pos[3];
				pos[0] = center;
				pos[1] = 10.5;
				pos[2] = 0.1f;
				gRenderDebug->setCurrentColor(0xFFFFFF,0xFFFFFF);
				gRenderDebug->setCurrentTextScale(2.0f);
				switch ( j )
				{
				case 0:
					gRenderDebug->debugText(pos,title1);
					break;
				case 1:
					gRenderDebug->debugText(pos,title2);
					break;
				}
				char scratch[512];
				sprintf(scratch,"Total Bitcoins: %s Total Non-Zero Addresses: %s", formatNumber(totalCoins), formatNumber(totalCount));
				pos[0] = center;
				pos[1] = 10.1;
				pos[2] = 0.1f;
				gRenderDebug->setCurrentColor(0xFFFF00,0xFFFFFF);
				gRenderDebug->setCurrentTextScale(1.5f);
				gRenderDebug->debugText(pos,scratch);
			}

			gRenderDebug->setCurrentTextScale(0.5f);

			for (uint32_t i=0; i<count; i++)
			{
				const UsageStat &u = stats1[i];

				float percent=0;
				float total=0;

				switch ( j )
				{
				case 0:
					percent = (float)u.mValue / (float) totalCoins;
					total = (float)u.mValue / 21000000.0f;
					break;
				case 1:
					percent = (float)u.mCount / (float) totalCount;
					total = (float)u.mValue / 21000000.0f;
					break;
				case 2:
					percent = (float)u.mCount / (float) totalCount;
					total = percent;
					break;
				}

				float x = i*BAR_SIZE+startX+BAR_SIZE;
				float wid = BAR_SIZE-(BAR_SIZE*0.1f);

				float bmin[3];
				float bmax[3];

				bmin[0] = x;
				bmax[0] = x+wid;

				bmin[1] = 0;
				bmax[1] = total*20.0f;

				bmin[2] = -wid;
				bmax[2] = 0;

				uint32_t color = gRenderDebug->getDebugColor((RENDER_DEBUG::DebugColors::Enum)c);
				gRenderDebug->setCurrentColor(color,0xFFFFFF);
				gRenderDebug->debugBound(bmin,bmax);


				gRenderDebug->setCurrentColor(0xFFFFFF,0xFFFFFF);
				float pos[3];
				pos[0] = bmin[0]+BAR_SIZE*0.5f;
				pos[1] = -0.2f;
				pos[2] = 0.1f;
				if ( strlen(u.mDescription) )
				{
					gRenderDebug->debugText(pos,u.mDescription);
					pos[1] = 0.1f;
					char scratch[512];
					sprintf(scratch,"%0.1f percent", percent*100 );
					gRenderDebug->debugText(pos,scratch);

					pos[1] = 0.3f;
					sprintf(scratch,"%s bitcoins", formatNumber(u.mValue) );
					gRenderDebug->debugText(pos,scratch);

					pos[1] = 0.5f;
					sprintf(scratch,"%s addresses", formatNumber(u.mCount) );
					gRenderDebug->debugText(pos,scratch);
				}


				c++;
				if ( c == RENDER_DEBUG::DebugColors::NUM_COLORS )
				{
					c = RENDER_DEBUG::DebugColors::Red;
				}
			}
		}
		gRenderDebug->render(1.0f/60.0f,NULL);
	}

	uint32_t getBTC(uint64_t btc)
	{
		uint64_t oneBtc = ONE_BTC;
		uint64_t result = btc / oneBtc;
		return (uint32_t)result;
	}

	virtual void processZombieReport(const char *dateString,const BlockChain::ZombieReport &report)
	{
		if ( mZombieReport == NULL )
		{
			mZombieReport = fopen("ZombieReport.csv", "wb");
			if ( mZombieReport )
			{
				fprintf(mZombieReport,"Date,CoinBase50,CoinBase25,NeverSpent,Normal,Dust,Alive\r\n");
			}
		}
		if ( mZombieReport )
		{
			fprintf(mZombieReport,"\"%s\",%d,%d,%d,%d,%d,%d\r\n",
				dateString,
				getBTC(report.mCoinBase50.mValue),
				getBTC(report.mCoinBase25.mValue),
				getBTC(report.mNeverSpent.mValue),
				getBTC(report.mNormal.mValue),
				getBTC(report.mDust.mValue),
				getBTC(report.mAlive.mValue) );
			fflush(mZombieReport);
		}
	}

	virtual void release(void) 
	{
		delete this;
	}

	bool	mFirst;
	FILE	*mFph;
	FILE	*mZombieReport;
};

DebugVisualize *createDebugVisualize(void)
{
	DebugVisualize *ret = NULL;
	getRenderDebugDLL();
	if ( gRenderDebug ) 
	{
		DebugVisualizeImpl *d = new DebugVisualizeImpl;
		ret = static_cast< DebugVisualize *>(d);
	}
	return ret;
}
blockchain.out: *.cpp *.h
	g++ -m32 -Wextra -Wall -Wno-unused -Wno-unknown-pragmas -g *.cpp -o blockchain.out
run:	blockchain.out
	./blockchain.out
